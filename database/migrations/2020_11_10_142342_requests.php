<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Requests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests',function(Blueprint $table){
            $table->id();
            $table->text('description')->nullable();;
            $table->float('weight');
            $table->string('state');
            $table->integer('volume')->nullable();
            $table->integer('number')->nullable();
            $table->timestamps();

            $table->foreignId('user_id')->constrained('users_data');
            $table->foreignId('trip_id')->constrained('trips');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
