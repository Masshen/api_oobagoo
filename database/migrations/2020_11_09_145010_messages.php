<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Messages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages',function(Blueprint $table){
            $table->id();
            $table->string('title')->nullable();;
            $table->string('content');
            $table->boolean('is_received');
            $table->datetime('received_at')->nullable();;
            $table->timestamps();

            $table->foreignId('sender_id')->constrained('users_data');
            $table->foreignId('receiver_id')->constrained('users_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
