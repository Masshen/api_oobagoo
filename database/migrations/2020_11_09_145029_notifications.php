<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Notifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications',function(Blueprint $table){
            $table->id();
            $table->text('content');
            $table->string('title')->nullable();;
            $table->boolean('is_received')->nullable();;
            $table->timestamps();

            $table->foreignId('user_id')->constrained('users_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
