<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Trips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips',function(Blueprint $table){
            $table->id();
            $table->float('available_weight');
            $table->datetime('start_date');
            $table->datetime('arrival_date');
            $table->text('description')->nullable();;
            $table->string('state');
            $table->float('price');
            $table->unsignedBigInteger('leave_id');
            $table->unsignedBigInteger('land_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('leave_id')->references('id')->on('towns');
            $table->foreign('land_id')->references('id')->on('towns');
            $table->foreign('user_id')->references('id')->on('users_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
