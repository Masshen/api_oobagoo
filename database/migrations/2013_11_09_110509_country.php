<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Countries;

class Country extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries',function(Blueprint $table){
            $table->id();
            $table->string('name');
            $table->string('lang')->nullable();
            $table->timestamps();
        });
        $list=[ 
            ["name"=>"RDC","lang"=>"fr"],
            ["name"=>"Senegal","lang"=>"fr"],
            ["name"=>"Djibouti","lang"=>"fr"],
        ];
        foreach ($list as $key => $value) {
            $country=new Countries;
            $country->name=$value['name'];
            $country->lang=$value['lang'];
            $country->save();
        }
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
