<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_data', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->date('born_date')->nullable();
            $table->string('adress1')->nullable();
            $table->string('adress2')->nullable();
            $table->string('cb')->nullable();
            $table->date('cb_date')->nullable();
            $table->string('cb_name')->nullable();
            $table->string('cb_number')->nullable();
            $table->string('cb_CVS')->nullable();
            $table->string('photo')->nullable();
            $table->string('phone')->nullable();
            $table->string('sex')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->text('presentation')->nullable();
            $table->integer('condition');
            $table->string('paypal')->nullable();
            $table->boolean('is_verified');
            $table->string('private_key');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_data');
    }
}
