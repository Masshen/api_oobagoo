<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Luggages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('luggages',function(Blueprint $table){
            $table->id();
            $table->datetime('accepted_on');
            $table->datetime('received_at')->nullable();
            $table->string('transmitted_photo')->nullable();
            $table->string('received_photo')->nullable();
            $table->string('state');
            $table->float('weight');
            $table->float('volume')->nullable();
            $table->integer('number')->nullable();
            $table->timestamps();

            $table->foreignId('receive_id')->constrained('users_data');
            $table->foreignId('travel_id')->constrained('users_data');
            $table->foreignId('send_id')->constrained('users_data');
            $table->foreignId('trip_id')->constrained('trips');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
