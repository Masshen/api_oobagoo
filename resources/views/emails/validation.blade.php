@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.newfeatureStart')

        <h3 class="secondary">Confirmation de votre compte <strong>ooBAGoo</strong></h3>
        <p><strong style="text-transform: uppercase;">{{ $user->last_name.' '}} </strong>
            <strong style="text-transform: capitalize;">{{ $user->first_name}} </strong>,
            pour profiter pleinement des fonctionnalités d ooBAGoo, vous devrez confirmer votre adresse émail!</p>

            @include('beautymail::templates.minty.button',
            ['text' => 'Cliquez pour confirmer votre adresse émail',
             'link' => url('/users/'.$user->private_key),
             'color'=>'#ace123'
             ])

    @include('beautymail::templates.widgets.newfeatureEnd')





@stop
