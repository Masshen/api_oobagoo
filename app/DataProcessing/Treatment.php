<?php
namespace App\DataProcessing;
use App\Models\User;

use function PHPUnit\Framework\isEmpty;
use Illuminate\Support\Str;

class Treatment{

    public static function getVerificationKey(){
        $passer=true;
        $list=User::get();
        $key='';
        do{
            $passer=true;
            $key=Str::random(120);
            $rep=$list->where('private_key',$key);
            if(!isEmpty($rep)){
                $passer=false;
            }
        }while(!$passer);

        return $key;
    }
}
