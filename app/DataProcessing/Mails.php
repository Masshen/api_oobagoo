<?php
namespace App\DataProcessing;
use App\Models\UsersData;
use Snowfire\Beautymail\Beautymail;
use Symfony\Component\Console\Input\Input;

class Mails{

    protected $user;
    public function __construct($user)
    {
        $this->user=$user;
    }

    public function sendUsersValidation(){

        $beautyMail=app()->make(Beautymail::class);
        $beautyMail->send('emails.validation',['user'=>$this->user],function($message){
            $message
                ->from('oobagoo@gmail.com')
                ->to($this->user->email)
                ->subject('ooBAGoo | Mail de confirmation');
        });
    }
}
