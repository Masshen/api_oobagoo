<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersData extends Model
{
    use HasFactory;
    protected $fillable = [
        'last_name','first_name','email','sex','adress1','adress2','phone',
        'country_id','is_verified','private_key','cb'
    ];
}
