<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Messages;
use App\Exceptions\Handler as Exception;
use DateTime;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $message=new Messages();
            $message->title=$request->title;
            $message->content=$request->message;
            $message->is_verified=false;
            $message->sender_id=$request->sender;
            $message->receiver_id=$request->receiver;
            $rep=$message->save();

            return response()->json(['state'=>$rep],200);

        }
        catch(Exception $e){

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $list=Messages::where('id',$id)->first();
            return response()->json($list,200);
        }
        catch(Exception $e){

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        if($request->title!=null){
            $data['title']=$request->title;
        }
        if($request->message!=null){
            $data['content']=$request->message;
        }
        if($request->is_received!=null){
            $date=new DateTime();
            $data['is_received']=$request->is_received;
            $data['received_at']=$request->$date;
        }
        if($data==null){
            return response()->json(['error'=>'Not to update',$data],404);
        }
        try{
            $message=Messages::find($id);
            $rep=$message->update($data);
            return response()->json(['state'=>$rep],200);
        }
        catch(Exception $e){
            return response()->json(['error'=>$e->error],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
