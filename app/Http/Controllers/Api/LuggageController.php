<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Luggages;
use App\Exceptions\Handler as Exception;
use Illuminate\Support\Facades\Validator;

class LuggageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $list=Luggages::get();
            return response()->json($list,200);
        }
        catch(Exception $e){

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=
        Validator::make($request->all(),[
            'weight'=>'required|numeric',
            'accepted_on'=>'required|date',
            'traveler'=>'required|exists:users_data,id',
            'receiver'=>'required|exists:users_data,id|different:traveler',
            'sender'=>'required|exists:users_data,id|different:traveler|different:receiver',
            'trip'=>'required|numeric|exists:trips,id',
        ]);

        if($validation->fails()){
            return response()->json(['error'=>$validation->getMessageBag()],404);
        }
        try{
            $luggage=new Luggages();
            $luggage->weight=$request->weight;
            $luggage->accepted_on=$request->accepted_on;
            $luggage->state=false;
            if($request->transmitted_photo!=null){
                $luggage->transmitted_photo=$request->transmitted_photo;
            }
            if($request->volume!=null){
                $luggage->volume=$request->volume;
            }
            if($request->number!=null){
                $luggage->number=$request->number;
            }
            $luggage->travel_id=$request->traveler;
            $luggage->receive_id=$request->receiver;
            $luggage->send_id=$request->sender;
            $luggage->trip_id=$request->trip;
            $rep=$luggage->save();
            return response()->json(['state'=>$rep]);
        }
        catch(Exception $e){

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $list=Luggages::where('id',$id)->first();
            return response()->json($list,200);
        }
        catch(Exception $e){

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        /**if the baggage weight is entered, and add to the list of data to modify */
        if($request->weight!=null){
            $data['weight']=$request->weight;
        }
        /**if the date of acceptance is subject to the modification, then we add in the list of data to modify */
        if($request->accepted_on!=null){
            $data['accepted_on']=$request->accepted_on;
        }
        /**if the date of receipt of baggage is entered then, we add in the list of data to modify */
        if($request->received_at!=null){
            $data['received_at']=$request->received_at;
        }
        /**if the baggage status is entered, add data to the list to modify */
        if($request->state!=null){
            $data['state']=$request->state;
        }
        /**when the baggage transmission photo is filled in, then it is added to the list of data to be modified */
        if($request->transmitted_photo!=null){
            $data['transmitted_photo']=$request->transmitted_photo;
        }
        /**when the photo of the baggage receipt is filled in, then it is added to the list of data to be modified */
        if($request->received_photo!=null){
            $data['received_photo']=$request->received_photo;
        }
        /**if the baggage volume is entered, then it is added to the list of data to be modified */
        if($request->volume!=null){
            $data["volume"]=$request->volume;
        }
        /**if the number of items in the baggage is entered, then it is added to the list of data to be modified */
        if($request->number!=null){
            $data['number']=$request->number;
        }
        if($data==null){
            return response()->json(['error'=>'Not to update',$data],404);
        }
        try{
            $luggage=Luggages::find($id);
            $rep=$luggage->update($data);
            return response()->json(['state'=>$rep],200);
        }
        catch(Exception $e){
            return response()->json(['error'=>$e->error],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
