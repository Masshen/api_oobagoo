<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\Handler as Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Countries;
use Illuminate\Support\Facades\Validator;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $list=Countries::get();
            return response()->json($list,200);
        }catch(Exception $e){

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=
        Validator::make($request->all(),[
            'name'=>'required|unique:countries'
        ]);
        if($validation->fails()){
            return response()->json(['error'=>$validation->getMessageBag()],404);
        }

        try{
            $country=new Countries;
            $country->name=$request->name;
            $country->lang=$request->lang;
            $rep=$country->save();

            return response()->json(['state'=>$rep]);
        }catch(Exception $e){

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $list=Countries::where('id',$id)->first();
            return response()->json($list,200);
        }catch(Exception $e){

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        if($request->name!=null){
            $data['name']=$request->name;
        }
        if($request->lang!=null){
            $data['lang']=$request->lang;
        }
        if($data==null){
            return response()->json(['error'=>'Not to update',$data],404);
        }
        try{
            $country=Countries::find($id);
            $rep=$country->update($data);
            return response()->json(['state'=>$rep],200);
        }
        catch(Exception $e){

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
