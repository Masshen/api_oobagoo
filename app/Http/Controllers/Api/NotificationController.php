<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notifications;
use App\Exceptions\Handler as Exception;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=
        Validator::make($request->all(),[
            'message'=>'required|min:1',
            'user'=>'required|numeric|exists:users_data,id'
        ]);


        if($validation->fails()){
            return response()->json(['error'=>$validation->getMessageBag()],404);
        }
        try {
            $notification=new Notifications();
            $notification->title=$request->title;
            $notification->content=$request->message;
            $notification->user_id=$request->user;
            $rep=$notification->save();
            return response()->json(['state'=>$rep],200);

        } catch (Exception $e) {
            //throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $list=Notifications::where('id',$id)->first();
            return response()->json($list,200);
        } catch (Exception $e) {
            //throw $th;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        if($request->title!=null){
            $data['title']=$request->title;
        }
        if($request->message!=null){
            $data['content']=$request->message;
        }
        if($request->is_received!=null){
            $data['is_received']=$request->is_received;
        }
        if($data==null){
            return response()->json(['error'=>'Not to update',$data],404);
        }
        try{
            $message=Notifications::find($id);
            $rep=$message->update($data);
            return response()->json(['state'=>$rep],200);
        }
        catch(Exception $e){
            return response()->json(['error'=>$e->error],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
