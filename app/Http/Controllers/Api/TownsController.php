<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Towns;
use App\Exceptions\Handler as Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TownsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $list=DB::table('towns')
                        ->join('countries','towns.country_id','=','countries.id')
                        ->select('towns.*','countries.name as country')
                        ->get();
            return response()->json($list,200);
        } catch (Exception $e) {
            //throw $th;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'name'=>'required|min:2',
            'country'=>'required|exists:countries,id|numeric'
        ]);
        if($validation->fails()){
            return response()->json(['error'=>$validation->getMessageBag()],404);
        }

        try {
            $town=new Towns();
            $town->nom=$request->name;
            $town->country_id=$request->country;
            $town->points=$request->points;
            $rep=$town->save();

            return response()->json(['state'=>$rep],200);
        } catch (Exception $th) {
            //throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $list=DB::table('towns')
                        ->join('countries','towns.country_id','=','countries.id')
                        ->select('towns.*','countries.name as country')
                        ->where('towns.id',$id)
                        ->get();
            return response()->json($list,200);
        } catch (Exception $e) {
            //throw $th;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        $rules=[];
        if($request->name!=null){
            $data['nom']=$request->name;
        }
        if($request->country!=null){
            $data['country_id']=$request->country;
            $rules['country_id']='required|exists:countries,id';
        }
        if($request->points!=null){
            $data['points']=$request->points;
        }
        if($request->state!=null){
            $data['state']=$request->state;
        }

        if($data==null){
            return response()->json(['error'=>'Not to update',$data],404);
        }
        $validation=Validator::make($data,$rules);
        if($validation->fails())
            return response()->json($validation->getMessageBag(),404);

        try{
            $town=Towns::find($id);
            $rep=$town->update($data);
            return response()->json(['state'=>$rep],200);
        }
        catch(Exception $e){
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
