<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UsersData as User;
use App\Exceptions\Handler as Exception;
use App\DataProcessing\Treatment;
use App\Mail\UsersData as MailUser;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\DataProcessing\Mails;

use function PHPUnit\Framework\isEmpty;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=User::get();
        return response()->json($list,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|unique:users_data|email',
            'phone'=>'',
            'password'=>'required',
            'country'=>'required|numeric|exists:countries,id'
        ]);
        if($validator->fails()){
            return response()->json(['error'=>$validator->getMessageBag()],403);
        }
        try{
            $user=new User;
            $user->first_name=$request->first_name;
            $user->last_name=$request->last_name;
            if($request->sex!=null)
                $user->sex=$request->sex;
            $user->email=$request->email;
            $user->phone=$request->phone;
            $user->password=$request->password;
            $user->is_verified=false;
            $user->private_key=Treatment::getVerificationKey();
            if($request->condition==null)
                $user->condition=0;
            else{
                if(!is_numeric($request->condition))
                    $user->condition=0;
                else
                    $user->condition=$request->condition;
            }
            $user->country_id=$request->country;
            $mail=new Mails($user);
            $mail->sendUsersValidation();
            $rep=$user->save();
            return response()->json(['state'=>$rep,'user'=>$user],200);
        }
        catch(Exception $e){

        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $list=User::where('id',$id)->first();
            return response()->json($list,200);
        }catch(Exception $e){
            return response()->json(['error'=>$e->error],404);
        }

    }
    public function search($content){
        try{
            $list=User::where('first_name','like',$content.'%')
                        ->orWhere('first_name','like','%'.$content)
                        ->orWhere('last_name','like',$content.'%')
                        ->orWhere('last_name','like','%'.$content)
                        ->get();
            return response()->json($list,200);
        }catch(Exception $e){

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateEmail=false;
        $data=[];
        /**add the first name, if this is prescribed  */
        if($request->first_name!=null){
            $data['first_name']=$request->first_name;
        }
        /**add the last name, if this is prescribed  */
        if($request->last_name!=null){
            $data['last_name']=$request->last_name;
        }
        /**add the first name, if this is prescribed  */
        if($request->sex!=null){
            $data['sex']=$request->sex;
        }
        /**add the phone number, if this is prescribed  */
        if($request->phone!=null){
            $data['phone']=$request->phone;
        }
        /**add the password, if this is prescribed  */
        if($request->password!=null){
            $data['password']=$request->password;
        }
        /**add the email, if this is prescribed  */
        if($request->email!=null){
            $data['email']=$request->email;
            $updateEmail=true;
        }
        /**add the first adress, if this is prescribed  */
        if($request->adress1!=null){
            $data['adress1']=$request->adress1;
        }
        /**add the second adress, if this is prescribed  */
        if($request->adress2!=null){
            $data['adress2']=$request->adress2;
        }
        /**add the date of birth, if prescribed  */
        if($request->born_date!=null){
            $data['born_date']=$request->born_date;
        }
        /**add the cb, if this is prescribed  */
        if($request->cb!=null){
            $data['cb']=$request->cb;
        }
        /**add the date of cb, if this is prescribed  */
        if($request->cb_date!=null){
            $data['cb_date']=$request->cb_date;
        }
        /**add the cb name, if this is prescribed  */
        if($request->cb_name!=null){
            $data['cb_name']=$request->cb_name;
        }
         /**add the cb number, if this is prescribed  */
         if($request->cb_name!=null){
            $data['cb_number']=$request->cb_number;
        }
         /**add the cb CVS, if this is prescribed  */
         if($request->cb_CVS!=null){
            $data['cb_CVS']=$request->cb_CVS;
        }
        /**add the user photo, if this is prescribed  */
        if($request->photo!=null){
            $data['photo']=$request->photo;
        }
        /**add the country, if this is prescribed  */
        if($request->country!=null){
            $data['country_id']=$request->country;
        }
        /**add the presentation, if this is prescribed  */
        if($request->presentation!=null){
            $data['presentation']=$request->presentation;
        }
        /**add the condition, if this is prescribed  */
        if($request->condition!=null){
            $data['condition']=$request->condition;
        }
        /**add the paypal, if this is prescribed  */
        if($request->paypal!=null){
            $data['paypal']=$request->paypal;
        }
        /**add the verification, if this is prescribed  */
        if($updateEmail==true){
            $data['is_verified']=false;
            $data['private_key']=Treatment::getVerificationKey();
        }
        if($data==null){
            return response()->json(['error'=>'Not to update',$data],404);
        }
        try{
            $user=User::find($id);
            if($updateEmail==true){
                $mail=new Mails($user);
                $mail->sendUsersValidation();
            }
            $rep=$user->update($data);
            return response()->json(['state'=>$rep],200);
        }
        catch(Exception $e){
            return response()->json(['error'=>$e->error],404);
        }

    }

    public function validation(Request $request,$key){
        try{
            $user=User::where('private_key',$key)->first();
            $rep=false;
            if(!$user->is_verified){
                $rep=$user->update(['is_verified'=>true]);
            }
            return response()->json(['state'=>$rep],200);
        }
        catch(Exception $e){

        }
    }
    public function authentification(Request $request){
        $validation=Validator::make($request->all(), [
            'email'=>'required|email',
            'password'=>'required'
        ]);
        if($validation->fails()){
            return response()->json($validation->getMessageBag(),404);
        }
        try{
            /**the first authentication concerns the email address and password*/
            $user=User::where('email',$request->email)
                ->where('password',$request->password)
                ->first();
            if($user!=null)
                return response()->json($user,200);
            else{
                $rep=["error"=>"Bad request, verify you password and email"];
                return response()->json($rep,400);
            }
        }
        catch(Exception $e){

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
