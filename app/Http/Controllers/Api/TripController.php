<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Trips;
use App\Models\UsersData as Users;
use App\Exceptions\Handler as Exception;
use Illuminate\Support\Facades\Validator;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $list=Trips::get();
            return response()->json($list,200);
        } catch (Exception $th) {
            //throw $th;
        }
    }

    /**
     * Display a listing of the user trips.
     *
     * @return \Illuminate\Http\Response
     */
    public function trips($id)
    {
        try {
            $list=Trips::where('users_id',$id);
            return response()->json($list,200);
        } catch (Exception $th) {
            //throw $th;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'available_weight'=>'required|numeric',
            'start_date'=>'required|date',
            'arrival_date'=>'required|date|different:start_date',
            'price'=>'required|numeric',
            'user'=>'required|exists:users_data,id',
            'town_start'=>'required|exists:towns,id',
            'town_arrival'=>'required|exists:towns,id|different:town_start',
        ]);
        if($validation->fails()){
            return response()->json(['error'=>$validation->getMessageBag()],404);
        }
        try {
            $trip=new Trips();
            $trip->available_weight=$request->available_weight;
            $trip->start_date=$request->start_date;
            $trip->arrival_date=$request->arrival_date;
            $trip->description=$request->description;
            $trip->state=$request->state;
            $trip->price=$request->price;
            $trip->user_id=$request->user;
            $trip->leave_id=$request->town_start;
            $trip->land_id=$request->town_arrival;

            $rep=$trip->save();
            return response()->json(['state'=>$rep,'trip'=>$trip],200);

        } catch (Exception $th) {
            //throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $list=Trips::where('id',$id)->first();
            return response()->json($list,200);
        } catch (Exception $th) {
            //throw $th;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        if($request->available_weight!=null){
            $data['available_weight']=$request->available_weight;
        }
        if($request->start_date!=null){
            $data['start_date']=$request->start_date;
        }
        if($request->arrival_date!=null){
            $data['arrival_date']=$request->arrival_date;
        }
        if($request->description!=null){
            $data['description']=$request->description;
        }
        if($request->state!=null){
            $data['']=$request->state;
        }
        if($request->price!=null){
            $data['']=$request->price;
        }
        if($request->user!=null){
            $data['user_id']=$request->user;
        }
        if($request->town_start!=null){
            $data['leave_id']=$request->town_start;
        }
        if($request->town_arrival!=null){
            $data['land_id']=$request->town_arrival;
        }
        if($data==null){
            return response()->json(['error'=>'Not to update',$data],404);
        }
        try{
            $trip=Trips::find($id);
            $rep=$trip->update($data);
            return response()->json(['state'=>$rep],200);
        }
        catch(Exception $e){
            return response()->json(['error'=>$e->error],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
