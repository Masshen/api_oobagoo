<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Requests as Demandes;
use App\Exceptions\Handler as Exception;
use Illuminate\Support\Facades\Validator;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $list=Demandes::get();
            return response()->json($list,200);
        } catch (Exception $e) {
            //throw $th;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=
        Validator::make($request->all(),[
            'weight'=>'required|numeric',
            'user'=>'required|numeric|exists:users_data,id',
            'trip'=>'required|numeric|exists:trips,id'
        ]);

        if($validation->fails()){
            return response()->json(['error'=>$validation->getMessageBag()],404);
        }

        try {
            $demande=new Demandes();
            $demande->weight=$request->weight;
            $demande->volume=$request->volume;
            $demande->number=$request->number;
            $demande->description=$request->description;
            $demande->user_id=$request->user;
            $demande->trip_id=$request->trip;
            $demande->state=false;
        } catch (Exception $e) {
            //throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $list=Demandes::where('id',$id)->get();
            return response()->json($list,200);
        } catch (Exception $e) {
            //throw $th;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        if($request->weight!=null){
            $data['weight']=$request->weight;
        }
        if($request->volume!=null){
            $data['volume']=$request->volume;
        }
        if($request->number!=null){
            $data['number']=$request->number;
        }
        if($request->description!=null){
            $data['description']=$request->description;
        }
        if($request->state!=null){
            $data['state']=$request->state;
        }
        if($data==null){
            return response()->json(['error'=>'Not to update',$data],404);
        }
        try{
            $demande=Demandes::find($id);
            $rep=$demande->update($data);
            return response()->json(['state'=>$rep],200);
        }
        catch(Exception $e){
            return response()->json(['error'=>$e->error],404);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
