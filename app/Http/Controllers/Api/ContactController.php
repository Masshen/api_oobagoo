<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contacts;
use App\Exceptions\Handler as Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=
        Validator::make($request->all(),[
            'user'=>'required|exists:users_data,id',
            'friend'=>'required|exists:users_data,id|different:user'
        ]);
        if($validation->fails()){
            return response()->json(['error'=>$validation->getMessageBag()],404);
        }

       try{
            $contact=new Contacts();
            $contact->user_id=$request->user;
            $contact->user_friend_id=$request->friend;
            if($request->description!=null){
                $contact->description=$request->description;
            }
            if($request->state!=null){
                $contact->state=$request->state;
            }else{
                $contact->state=false;
            }
            $rep=$contact->save();
            return response()->json(['state'=>$rep],200);
       }
       catch(Exception $e){

       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $list=DB::table('users_data')
                    ->join('contacts','users_data.id','=','contacts.user_friend_id')
                    ->select('users_data.*')
                    ->where('contacts.user_id',$id)
                    ->get();
            return response()->json($list,200);
        }
        catch(Exception $e){

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
