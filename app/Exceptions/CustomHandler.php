<?php

namespace App\Exceptions;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\QueryException;
use BadMethodCallException;

use Exception;

class CustomHandler extends Exception
{
    public function report(Exception $e)
    {
        if($e instanceof ModelNotFoundException){
            return response()->json(['error'=>'Not found',404]);
        }
        if($e instanceof BindingResolutionException){
            return response()->json(['error'=>'Binding',404]);
        }
        if($e instanceof QueryException){
            return response()->json(['error'=>'Query',404]);
        }
        if($e instanceof BadMethodCallException){
            return response()->json(['error'=>'Query',404]);
        }
    }

    public function render($request,Exception $e){
        if($e instanceof ModelNotFoundException){
            return response()->json(['error'=>'Not found',401]);
        }
        if($e instanceof BindingResolutionException){
            return response()->json(['error'=>'Binding',404]);
        }
        if($e instanceof QueryException){
            return response()->json(['error'=>'Query',401]);
        }
        //return parent::render($request,$e);
    }
}
