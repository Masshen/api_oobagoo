<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Swift_TransportException;
use Exception;
use Illuminate\Http\Request;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    public function render($request, Throwable $e)
    {
        if($e instanceof ModelNotFoundException){
            return response()->json(['error'=>'Not found'],404);
        }
       /* if($e instanceof BindingResolutionException){
            return response()->json(['error'=>'Binding'],404);
        }*/
        if($e instanceof QueryException){
            return response()->json(['error'=>$e->getMessage()],404);
        }
        if($e instanceof MassAssignmentException){
            return response()->json(['error'=>$e->getMessage()],404);
        }
        if($e instanceof Swift_TransportException){
            return response()->json(['error'=>$e->getMessage()],404);
        }
        return parent::render($request,$e);
    }


    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
