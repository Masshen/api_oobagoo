<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ContactController;
use App\Http\Controllers\Api\CountryController;
use App\Http\Controllers\Api\LuggageController;
use App\Http\Controllers\Api\MessageController;
use App\Http\Controllers\Api\NotificationController;
use App\Http\Controllers\Api\RequestController;
use App\Http\Controllers\Api\TownsController;
use App\Http\Controllers\Api\TripController;
use App\Http\Controllers\Api\UsersController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*
|-------------------------------------------------------------------------
|1. MANAGE USERS
|-------------------------------------------------------------------------
*/
Route::resource('users',UsersController::class)->except(['create','edit']);
Route::get('users/search/{content}',[UsersController::class,'search'])->name('users.search');
Route::get('users/authentification/{identity}',[UsersController::class,'authentification'])->name('users.autentification');
Route::post('users/authentification',[UsersController::class,'authentification'])->name('users.authentification');

/*
|-------------------------------------------------------------------------
|2. MANAGE CONTACTS
|-------------------------------------------------------------------------
*/
Route::resource('contacts',ContactController::class)->except(['create','edit','index']);

/*
|-------------------------------------------------------------------------
|3. MANAGE COUNTRY
|-------------------------------------------------------------------------
*/
Route::resource('country',CountryController::class)->except(['create','edit','destroy']);

/*
|-------------------------------------------------------------------------
|4. MANAGE LUGGAGE
|-------------------------------------------------------------------------
*/
Route::resource('bags',LuggageController::class)->except(['create','edit']);

/*
|-------------------------------------------------------------------------
|5. MANAGE MESSAGE
|-------------------------------------------------------------------------
*/
Route::resource('message',MessageController::class)->except(['create','edit','index']);

/*
|-------------------------------------------------------------------------
|6. MANAGE NOTIFICATION
|-------------------------------------------------------------------------
*/
Route::resource('notifications',NotificationController::class)->except(['create','edit']);

/*
|-------------------------------------------------------------------------
|7. MANAGE REQUEST
|-------------------------------------------------------------------------
*/
Route::resource('request',RequestController::class)->except(['create','edit']);


/*
|-------------------------------------------------------------------------
|8. MANAGE TOWNS
|-------------------------------------------------------------------------
*/
Route::resource('towns',TownsController::class)->except(['create','edit','destroy']);

/*
|-------------------------------------------------------------------------
|9. MANAGE TRIP
|-------------------------------------------------------------------------
*/
Route::resource('trips',TripController::class)->except(['create','edit','destroy']);
Route::get('trips/user/{id}',[TripController::class,'trips']);
